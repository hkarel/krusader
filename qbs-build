#!/bin/bash

set -u

jobs=$(nproc)
[ "$jobs" -gt 24 ] && jobs=$(($jobs - 6))
echo "jobs: $jobs"

rebuild=
print_help=
build_mode=release
need_packages=

build_deb_package=

install_krusader=
remove_krusader=

# Directory of this script
script_dir=$(dirname $(readlink -f $0))
echo "script_dir: $script_dir"

# Defining host parameters
if [ ! -e $script_dir/setup/os_detect ]; then
    echo "Error: os_detect script not found"
    exit 1
fi
. $script_dir/setup/os_detect


function display_help()
{
cat << EOF
Usage: ${0##*/} [OPTION]
  -h   display this help and exit
  -r   full rebuild of project
  -d   build in 'debug' mode
  -D   build deb packages
  -i   install krusader-test
  -R   remove installation krusader-test and exit

EOF
}

# Positional parameter processing:
#    http://wiki.bash-hackers.org/scripting/posparams
while test -n ${1:-""}
do
    case "$1" in
        -h)
            print_help=yes
            shift
            ;;
        -r)
            rebuild=yes
            shift
            ;;
        -d)
            build_mode=debug
            shift
            ;;
        -D)
            build_deb_package=yes
            shift
            ;;
        -i)
            install_krusader=yes
            shift
            ;;
        -R)
            remove_krusader=yes
            shift
            ;;

        -*)
            echo "Error: Unknown option: $1" >&2
            exit 1
            ;;
        *)  # No more options
            break
            ;;
    esac
done

if [ "$print_help" = "yes" ]; then
    display_help
    exit 0
fi

if [ "$remove_krusader" = "yes" ]; then
    echo -e "\nRemove Krusader (test)..."

    set +e
    res=$(dpkg -l | grep -P '^ii\s+krusader-test ')
    set -e
    if [ -z "$res" ]; then
        echo "Nothing to remove, Krusader (test)... not installed"
    else
        sudo dpkg -r krusader-test
        echo "Krusader (test)... was removed"
    fi
fi
[ "$remove_krusader" = "yes" ] && exit 0;

function check_need_package()
{
    dpkg -s $1 &> /dev/null
    [ "$?" -ne 0 ] && need_packages="$need_packages $1"
}

need_packages=
check_need_package 'qt5-default'
check_need_package 'qtbase5-dev'
check_need_package 'qtbase5-dev-tools'
check_need_package 'qtbase5-private-dev'

# Required packages for Krusader-building
check_need_package 'gettext'
check_need_package 'zlib1g-dev'
check_need_package 'libkf5archive-dev'
check_need_package 'libkf5doctools-dev'
check_need_package 'libkf5kio-dev'
check_need_package 'libkf5notifications-dev'
check_need_package 'libkf5parts-dev'
check_need_package 'libkf5wallet-dev'
check_need_package 'libkf5xmlgui-dev'

if [ "$build_deb_package" = "yes" -o "$install_krusader" = "yes" ]; then
    # Required packages for DEB-building
    check_need_package 'fakeroot'
    check_need_package 'debconf'
    check_need_package 'debhelper'
    check_need_package 'lintian'

    if [ "$os_id" = "ubuntu" -a "$os_ver" \> "15.10" ]; then
        check_need_package 'hashdeep'
    fi
fi

if [ -n "$need_packages" ]; then
    echo "Error: Need to install packages:$need_packages"
    echo "    Use: sudo apt-get install $need_packages"
    exit 1
fi

if [ ! -x /usr/bin/md5deep ]; then
    if [ ! -L /usr/local/bin/md5deep ]; then
        echo "Need create symlink for md5deep"
        sudo ln -sf /usr/bin/hashdeep /usr/local/bin/md5deep
    fi
fi

git_branch=$(git status -b -s | head -n1 | sed 's/\./ /' | cut -d' ' -f2 | sed 's/\//_/g')
build_subdir=${build_mode}-${git_branch}
build_dir=./build/$build_subdir

[ "$rebuild" = "yes"  ] && rm -rf $build_dir

set -e
qbs build \
    --file krusader_project.qbs \
    --build-directory ./build \
    --command-echo-mode command-line \
    --jobs $jobs \
    --no-install \
    qbs.buildVariant:$build_mode \
    config:$build_subdir
    #profile:qt5sys

not_exit=
if [ "$build_deb_package" = "yes" -o "$install_krusader" = "yes" ]; then
    not_exit=yes
fi
[ "$not_exit" != "yes" ] && exit 0

#
# Creating a deb-package
#
package_vers=$(head -n1 VERSION)
echo "package_vers: $package_vers"

#package_date=$(date +%Y%m%d%H%M)
package_date=$(date +%Y%m%d)
echo "package_date: $package_date"

gitrev=$(git log -1 --pretty=%h)
echo "gitrev: $gitrev"

if [ "$build_deb_package" != "yes" -a "$install_krusader" != "yes" ]; then
    exit 0
fi

#
# Generating deb-package
#
package_dir=$script_dir/build/${build_subdir}/deb/krusader
echo -e "\npackage_dir: $package_dir"

package_name="krusader-test-${package_vers}-${package_date}git${gitrev}-${os_id}${os_ver}-${os_arch}.deb"
echo "package_name: $package_name"

rm -rf $package_dir

mkdir -p $package_dir/DEBIAN
mkdir -p $package_dir/usr/bin

cp $script_dir/setup/deb/krusader/DEBIAN/*  $package_dir/DEBIAN
cp $build_dir/bin/krusader                  $package_dir/usr/bin/krusader-test

chmod -R go-w        $package_dir
chmod    u=rwx,go=rx $package_dir/DEBIAN/post*
chmod    u=rwx,go=rx $package_dir/DEBIAN/pre*
chmod    u=rwx,go=rx $package_dir/usr/bin/krusader-test

if [ "$build_mode" = "release" ]; then
    set +e
    echo "Removing debug info ... "
    strip --strip-debug --strip-unneeded $package_dir/usr/bin/krusader-test
    set -e
fi

# Packet size
installed_size=$(du -s $package_dir | sed -r 's/([0-9]+).*/\1/')
echo "installed_size: $installed_size"

os_arch_control=$os_arch
[ "${os_arch:0:3}" = "arm" ] && os_arch_control=armhf
sed -e "s/%VERSION%/${package_vers}-${package_date}git${gitrev}/" \
    -e "s/%ARCHITECTURE%/${os_arch_control}/" \
    -e "s/%INSTALLED_SIZE%/${installed_size}/" \
    -i $package_dir/DEBIAN/control

# Deb-package checksums
cd $package_dir
md5deep -rl -o f usr >> DEBIAN/md5sums
chmod  0644 DEBIAN/md5sums
cd $script_dir

# Creating deb-package
fakeroot dpkg-deb --build $package_dir ${build_dir}/$package_name

# Check deb-package
echo "Start 'lintian'"
set +e
lintian --suppress-tags \
hardening-no-relro,\
binary-or-shlib-defines-rpath,\
dir-or-file-in-opt,\
bad-package-name,\
package-not-lowercase,\
systemd-service-file-outside-lib,\
maintainer-script-calls-systemctl,\
file-in-etc-not-marked-as-conffile,\
maintainer-script-ignores-errors,\
maintainer-script-empty,\
file-in-unusual-dir \
${build_dir}/$package_name
set -e

if [ "$build_deb_package" = "yes" ]; then
    cd $script_dir
    mkdir -p packages
    echo "Copying the file $package_name to directory ./packages"
    cp -f ${build_dir}/$package_name ./packages
fi

if [ "$install_krusader" = "yes" ]; then
    echo "Install Krusader (test)..."
    sudo dpkg -i ${build_dir}/$package_name
    echo "Krusader (test) installation is complete"
fi
