import qbs
import qbs.File
import qbs.TextFile
import qbs.Process
//import "qbs/imports/QbsUtl/qbsutl.js" as QbsUtl

Project {
    name: "Krusader (Project)"

    minimumQbsVersion: "1.21.0"
    qbsSearchPaths: ["qbs"]

    readonly property string projectVersion: projectProbe.projectVersion
    readonly property string projectReleaseName: projectProbe.projectReleaseName
    readonly property string projectGitRevision: projectProbe.projectGitRevision

    Probe {
        id: projectProbe
        property string projectVersion;
        property string projectReleaseName;
        property string projectGitRevision;

        readonly property string projectBuildDirectory:  project.buildDirectory
        readonly property string projectSourceDirectory: project.sourceDirectory

        configure: {
            var krvers = new TextFile(projectSourceDirectory + "/VERSION", TextFile.ReadOnly);
            try {
                projectVersion = krvers.readLine().trim();     // First line
                projectReleaseName = krvers.readLine().trim(); // Second line
            }
            finally {
                krvers.close();
            }

            File.makePath(projectBuildDirectory);

            krvers = new TextFile(projectBuildDirectory + "/krusaderversion.h" , TextFile.WriteOnly);
            krvers.write("// Dummy file, required for compatibility with CMAKE build system\n");
            krvers.close();

            var process = new Process();
            try {
                process.setWorkingDirectory(projectSourceDirectory);
                if (process.exec("git", ["log", "-1", "--pretty=%h"], false) === 0)
                    projectGitRevision = process.readLine().trim();
            }
            finally {
                process.close();
            }
        }
    }

    property var cppDefines: {
        var def = [
            "RELEASE_NAME=\"" + projectReleaseName + "\"",
            "VERSION=\"" + projectVersion + "\"",
            "GIT_REVISION=\"" + projectGitRevision + "\"",
            "KDESU_PATH=\"/usr/lib/x86_64-linux-gnu/libexec/kf5/kdesu\"",
            "SYNCHRONIZER_ENABLED",
            "KRARC_ENABLED",
            "KRARC_QUERY_ENABLED",
        ];

        if (qbs.buildVariant === "release")
            def.push("NDEBUG");

        return def;
    }

    property var cxxFlags: {
        var f = [
            "-ggdb3",
            "-Wall",
            "-Wextra",
            "-Wno-unused-parameter",
            //"-Wno-variadic-macros",
        ];
        //if (qbs.buildVariant === "release")
        //    f.push("-s");
        //else
        //    f.push("-ggdb3");

        return f;
    }
    property string cxxLanguageVersion: "c++17"

    references: [
        "plugins/iso/iso.qbs",
        "plugins/krarc/krarc.qbs",
        "app/krusader.qbs",
    ]

}

/** Пакеты **
libkf5config-dev
libkf5kdelibs4support-dev
libkf5kio-dev
libkf5parts-dev
libkf5xmlgui-dev
libkf5widgetsaddons-dev
libkf5i18n-dev
libkf5service-dev
libkf5completion-dev
libkf5configwidgets-dev
libkf5sonnet-dev
libkf5notifications-dev
libkf5archive-dev
libkf5jobwidgets-dev
libkf5solid-dev
libkf5bookmarks-dev
libkf5textwidgets-dev
libkf5codecs-dev
libkf5guiaddons-dev
libkf5itemviews-dev
libkf5wallet-dev
*/
