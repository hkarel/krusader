import qbs
import qbs.ModUtils

Product {
    name: "KrArc"
    targetName: "krarc"
    condition: true

    //type: "dynamiclibrary"
    type: "staticlibrary"
    destinationDirectory: "./lib"

    Depends { name: "cpp" }
    Depends { name: "Qt"; submodules: ["core", "gui", "network", ] }

    cpp.defines: project.cppDefines;
    cpp.cxxFlags: project.cxxFlags.concat(["-fPIC"])
    cpp.cxxLanguageVersion: project.cxxLanguageVersion

//    cpp.includePaths: [
//        ".",
//        "..",
//    ]

    cpp.systemIncludePaths: ModUtils.concatAll(
        Qt.core.cpp.includePaths
       ,"/usr/include/KF5"
       ,"/usr/include/KF5/KIOCore"
       ,"/usr/include/KF5/KConfigCore"
       ,"/usr/include/KF5/KCoreAddons"
       ,"/usr/include/KF5/KArchive"
       ,"/usr/include/KF5/KI18n"
    )

    cpp.dynamicLibraries: [
        "pthread",
        "KF5KIOCore",
        "KF5ConfigCore",
        "KF5CoreAddons",
        "KF5I18n",
    ]

    files: [
        "*.cpp",
        "*.h",
    ]

    Export {
        Depends { name: "cpp" }
        cpp.includePaths: "."
    }

} // Product
