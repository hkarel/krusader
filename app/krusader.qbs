import qbs
import qbs.ModUtils

Product {
    name: "Krusader"
    targetName: "krusader"
    condition: true

    type: "application"
    destinationDirectory: "./bin"

    Depends { name: "cpp" }
    Depends { name: "Iso" }
    Depends { name: "KrArc" }
    Depends { name: "Qt"; submodules: ["core", "gui", "widgets", "network", "xml",
                                       "printsupport", "dbus" ] }

    cpp.defines: project.cppDefines;
    cpp.cxxFlags: project.cxxFlags
    cpp.cxxLanguageVersion: project.cxxLanguageVersion

    cpp.includePaths: [
        ".",
        "..",
        "Archive",
        "DiskUsage",
        "DiskUsage/filelightParts",
        "FileSystem",
        "Panel",
        project.buildDirectory,
    ]

    cpp.systemIncludePaths: ModUtils.concatAll(
        Qt.core.cpp.includePaths
       ,"/usr/include/KF5"
       ,"/usr/include/KF5/KIOCore"
       ,"/usr/include/KF5/KParts"
       ,"/usr/include/KF5/KConfigCore"
       ,"/usr/include/KF5/KConfigGui"
       ,"/usr/include/KF5/KConfigWidgets"
       ,"/usr/include/KF5/KCoreAddons"
       ,"/usr/include/KF5/KCodecs"
       ,"/usr/include/KF5/KGuiAddons"
       ,"/usr/include/KF5/KWidgetsAddons"
       ,"/usr/include/KF5/KXmlGui"
       ,"/usr/include/KF5/KArchive"
       ,"/usr/include/KF5/KItemViews"
       ,"/usr/include/KF5/KIconThemes"
       ,"/usr/include/KF5/KIOWidgets"
       ,"/usr/include/KF5/KJobWidgets"
       ,"/usr/include/KF5/KIOFileWidgets"
       ,"/usr/include/KF5/KTextWidgets"
       ,"/usr/include/KF5/KNotifications"
       ,"/usr/include/KF5/KBookmarks"
       ,"/usr/include/KF5/KDELibs4Support/KDE"
       ,"/usr/include/KF5/KWindowSystem"
       ,"/usr/include/KF5/KWallet"
       ,"/usr/include/KF5/KI18n"
       ,"/usr/include/KF5/KService"
       ,"/usr/include/KF5/Solid"
       ,"/usr/include/KF5/KCompletion"
       ,"/usr/include/KF5/SonnetUi"
    )

    cpp.rpaths: ModUtils.concatAll(
        "$ORIGIN/../lib"
    )

//     cpp.libraryPaths: ModUtils.concatAll(
//         lib.firebird.libraryPath
//         //project.buildDirectory + "/lib"
//     )

    cpp.dynamicLibraries: [
        "pthread",
        "KF5KIOCore",
        "KF5ConfigCore",
        "KF5CoreAddons",
        "KF5ConfigGui",
        "KF5GuiAddons",
        "KF5ConfigWidgets",
        "KF5KIOWidgets",
        "KF5TextWidgets",
        "KF5KIOFileWidgets",
        "KF5WidgetsAddons",
        "KF5Service",
        "KF5Completion",
        "KF5WindowSystem",
        "KF5JobWidgets",
        "KF5ItemViews",
        "KF5KDELibs4Support",
        "KF5I18n",
        "KF5Codecs",
        "KF5IconThemes",
        "KF5Parts",
        "KF5XmlGui",
        "KF5Wallet",
        "KF5Bookmarks",
        "KF5Solid",
        "KF5Notifications",
        "KF5Archive",
    ]

    Group {
        name: "resources"
        files: "resources.qrc"
    }

    files: {
        var f = [
            "ActionMan/*.cpp",
            "ActionMan/*.h",
            "ActionMan/*.ui",
//            "Archive/../../krArc/krarcbasemanager.cpp",
//            "Archive/../../krArc/krarcbasemanager.h",
//            "Archive/../../krArc/krlinecountingprocess.cpp",
//            "Archive/../../krArc/krlinecountingprocess.h",
            "Archive/*.cpp",
            "Archive/*.h",
            "BookMan/*.cpp",
            "BookMan/*.h",
            "Dialogs/*.cpp",
            "Dialogs/*.h",
            "Dialogs/*.ui",
            "DiskUsage/filelightParts/*.cpp",
            "DiskUsage/filelightParts/*.h",
            "DiskUsage/radialMap/*.cpp",
            "DiskUsage/radialMap/*.h",
            "DiskUsage/*.cpp",
            "DiskUsage/*.h",
            "FileSystem/*.cpp",
            "FileSystem/*.h",
            "Filter/*.cpp",
            "Filter/*.h",
            "GUI/*.cpp",
            "GUI/*.h",
            //icons
            "JobMan/*.cpp",
            "JobMan/*.h",
            "Konfigurator/*.cpp",
            "Konfigurator/*.h",
            //"KrJS/*.cpp",
            //"KrJS/*.h",
            "KViewer/*.cpp",
            "KViewer/*.h",
            "Locate/*.cpp",
            "Locate/*.h",
            "MountMan/*.cpp",
            "MountMan/*.h",
            "Panel/PanelView/*.cpp",
            "Panel/PanelView/*.h",
            "Panel/*.cpp",
            "Panel/*.h",
            "Search/*.cpp",
            "Search/*.h",
            "Splitter/*.cpp",
            "Splitter/*.h",
            "Synchronizer/*.cpp",
            "Synchronizer/*.h",
            "UserAction/*.cpp",
            "UserAction/*.h",
            "*.cpp",
            "*.h",
        ];
        return f;
    }

} // Product
